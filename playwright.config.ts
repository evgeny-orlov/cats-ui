import { devices } from '@playwright/test';

/**
 * В целом дока pw https://playwright.dev/docs/intro
 * Параметры конфига https://playwright.dev/docs/test-configuration
 */
const config = {
  testMatch:  process.env.STAND ? /src\/pages\/[^\/]+\/__tests__\/.*\.test\.ts/ :  /.*\/components\/.*\/__tests__\/.*\.test\.ts/,

  /**
   * Про репортеры https://playwright.dev/docs/test-reporters
   */
  reporter: [
    ['html', { open: 'never' }],
    ['allure-playwright', { outputFolder: 'allure-results' }],
  ],

  use: {
    baseURL: process.env.STAND ? process.env.STAND : 'http://localhost:6006',
    /**
     * Про таймауты https://playwright.dev/docs/test-timeouts
     */
    navigationTimeout: 15_000,
    screenshot: 'only-on-failure',

    /**
     * Трейс тестов https://playwright.dev/docs/trace-viewer
     */
    trace: 'retain-on-failure',
  },

  expect: {
    timeout: 5_000,
  },

  projects: [
    {
      name: 'chromium',
      use: { ...devices['Desktop Chrome'] },
    },
    {
      name: 'safari',
      use: { browserName: 'webkit' },
    },
  ],
};

export default config