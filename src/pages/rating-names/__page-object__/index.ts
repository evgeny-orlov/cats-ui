import { Page, test, TestFixture } from '@playwright/test';

export class RatingsPage {
    private page: Page;

    constructor({ page }: { page: Page; }) {
        this.page = page;
    }

    async openRatingsPage() {
        return await test.step('Открыть страницу рейтинга котиков', async () => {
            await this.page.goto('/rating');
        });
    }

    async getCatsRatings(): Promise<{ name: string, likes: number }[]> {
        return await test.step('Получить рейтинг котиков по лайкам', async () => {
            const rows = await this.page.$$('//html/body/div[1]/div/section[2]/div/div/div[2]/div[2]/table[1]/tbody/tr');
            const ratings = [];
            for (const row of rows) {
                const name = await row.$eval('.rating-names_item-name__1OPc8 a', el => el.textContent.trim());
                const likes = await row.$eval('.rating-names_item-count__1LGDH', el => parseInt(el.textContent.trim()));
                ratings.push({name, likes});
            }
            return ratings;
        });
    }

    async errorMessage() {
        return this.page.getByText('Ошибка загрузки рейтинга');
    }
}

export type RatingsPageFixture = TestFixture<
    RatingsPage,
    { page: Page; }
>;

export const ratingsPageFixture: RatingsPageFixture = async ({ page }, use) => {
    const ratingsPage = new RatingsPage({ page });
    await use(ratingsPage);
};

