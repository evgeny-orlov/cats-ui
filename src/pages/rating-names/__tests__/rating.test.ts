import { test as base, expect } from '@playwright/test';
import { RatingsPage, ratingsPageFixture } from '../__page-object__';

const test = base.extend<{ ratingsPage: RatingsPage }>({
  ratingsPage: ratingsPageFixture,
});

test('При ошибке сервера в методе rating - отображается попап ошибки', async ({
  page,
  ratingsPage,
}) => {
  await page.route(
    request => request.href.includes('/api/likes/cats/rating'),
    async route => {
      await route.fulfill({
        status: 500,
        body: JSON.stringify({ error: 'Internal Server Error' }),
      });
    }
  );

  await ratingsPage.openRatingsPage();

  const errorMessage = await ratingsPage.errorMessage();
  await expect(errorMessage).toBeVisible();
});

test('Проверка сортировки котиков по лайкам', async ({ ratingsPage }) => {
  await ratingsPage.openRatingsPage();
  const ratings = await ratingsPage.getCatsRatings();

  const likes = ratings.map(r => r.likes);
  const sortedLikes = [...likes].sort((a, b) => b - a);
  expect(likes).toEqual(sortedLikes);
});
